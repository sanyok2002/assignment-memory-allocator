#include "tests.h"
#include <stdio.h>
#include <string.h>
#include "mem.h"
#include "mem_internals.h"


static void* init(){
    void *heap = heap_init(10000);

    debug_heap(stdout, heap);

    return heap;
}

void test_1(struct block_header *heap){
    printf("---------------test 1----------------\n");
    void *block = _malloc(1000);

    debug_heap(stdout, heap);

    _free(block);

    debug_heap(stdout, heap);
    printf("\n\n\n");
}

void test_2(struct block_header *heap){
    printf("---------------test 2----------------\n");
    void *block1 = _malloc(1000);
    void *block2 = _malloc(1000);

    debug_heap(stdout, heap);

    _free(block1);

    debug_heap(stdout, heap);

    _free(block2);

    debug_heap(stdout, heap);
    printf("\n\n\n");
}

void test_3(struct block_header *heap){
    printf("---------------test 3----------------\n");
    void *block1 = _malloc(1000);
    void *block2 = _malloc(1000);

    debug_heap(stdout, heap);

    _free(block1);
    _free(block2);

    debug_heap(stdout, heap);

    void *block3 = _malloc(1000);

    debug_heap(stdout, heap);

    _free(block3);

    debug_heap(stdout, heap);
    printf("\n\n\n");
}

void test_4(struct block_header *heap){
    printf("---------------test 4----------------\n");
    void *block1 = _malloc(9000);
    debug_heap(stdout, heap);

    void *block2 = _malloc(5000);
    debug_heap(stdout, heap);

    _free(block1);
    _free(block2);

    debug_heap(stdout, heap);
    printf("\n\n\n");
}

void test_5(struct block_header *heap){
    printf("---------------test 5----------------\n");
    void *block1 = _malloc(1000);
    debug_heap(stdout, heap);

    void *block2 = _malloc(5000);
    debug_heap(stdout, heap);

    _free(block1);
    debug_heap(stdout, heap);

    void *block3 = _malloc(5000);

    debug_heap(stdout, heap);

    _free(block2);
    _free(block3);
    debug_heap(stdout, heap);
    printf("\n\n\n");
}
void run_all_tests(){
    void * heap = init();
    test_1(heap);
    test_2(heap);
    test_3(heap);
    test_4(heap);
    test_5(heap);
}
